'use strict';

const express = require('express');
const ejs = require('ejs');

// Constants
const PORT = 3000;

// App
const app = express();
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const template = `
<html>
    <body>
        <h1>Request Info</h1>
        <dl>
            <dt>Path</dt>
            <dd><%= req.path %></dd>
        </dl>
        <dl>
            <dt>Method</dt>
            <dd><%= req.method %></dd>
        </dl>
        <dl>
            <dt>Query params</dt>
            <dd>
                <ul> 
                    <%for (var p in req.query) {%>
                        <li><%= p %> : <%= req.query[p]%></li>
                    <%} %>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt>Body params</dt>
            <dd>
                <ul> 
                    <%for (var p in req.body) {%>
                        <li><%= p %> : <%= req.body[p]%></li>
                    <%} %>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt>Headers</dt>
            <dd>
                <ul> 
                    <%for (var header in req.headers) {%>
                        <li><%= header %> : <%= req.get(header)%></li>
                    <%} %>
                </ul>
            </dd>
        </dl>
    </body>
</html>
`;

app.all('/*', (req, res) => {
    res.send(ejs.render(template, { req: req }));
});

app.listen(PORT);
console.log(`Running on http://${PORT}`);